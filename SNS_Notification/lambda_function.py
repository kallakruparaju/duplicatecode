import os
import boto3

def lambda_handler(event, context):
    # Retrieve the SNS topic ARN from the environment variable
    topic_arn = os.environ['SNS_TOPIC_ARN']

    # Create an SNS client
    sns_client = boto3.client('sns')

    # Replace 'Your Message' with the message you want to publish
    message = 'Your Message'

    try:
        # Publish the message to the specified SNS topic
        response = sns_client.publish(
            TopicArn=topic_arn,
            Message=message
        )

        # Print the message ID for reference
        print("Message published successfully. Message ID:", response['MessageId'])
        return {
            'statusCode': 200,
            'body': 'Message published successfully.'
        }
    except Exception as e:
        print("Error publishing message:", str(e))
        return {
            'statusCode': 500,
            'body': 'Error publishing message.'
        }